#!/bin/bash

# Set the DISPLAY variable to the correct X display.
export DISPLAY=:0

# Identify the HDMI sink name
HDMI_SINK_NAME=$(pactl list short sinks | grep -i 'hdmi' | awk '{print $2}')

# Set HDMI playback volume to 100% using the identified sink.
if [ -n "$HDMI_SINK_NAME" ]; then
  pactl set-sink-volume "$HDMI_SINK_NAME" 100%
  echo "HDMI playback volume set to 100% for sink $HDMI_SINK_NAME."
else
  echo "HDMI sink not found."
fi

# Use pkill to kill all instances of mpv
pkill mpv

# Remove old log file
rm /mnt/video_jukebox_data/mpv.log

# Remove old playlist

rm /tmp/morning_playlist.txt

# Directory containing video files
video_dir="/mnt/video_jukebox_data/morning"

# Create a new playlist file with the list of video files in the directory
find "$video_dir" -type f -name "*.mp4" -o -name "*.mkv" -o -name "*.avi" > /tmp/morning_playlist.txt

# Randomize the playlist
shuf /tmp/morning_playlist.txt -o /tmp/morning_playlist.txt

# Run mpv with above playlist, full screen using pulse audio, logging, and socket server.
mpv --input-ipc-server=/tmp/mpv-socket --log-file=/mnt/video_jukebox_data/mpv.log --fs=yes --pulse-host= --playlist=/tmp/morning_playlist.txt --volume=80

