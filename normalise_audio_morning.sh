#!/bin/bash

#This script uses the loudnorm audio filter to normalize the audio to a target integrated loudness level of -24 LUFS, a loudness range of 7, and a true peak value of -1.5 dB. You can adjust these parameters as needed for your specific requirements.

# Check if ffmpeg is installed
if ! command -v ffmpeg &> /dev/null; then
    echo "ffmpeg is not installed. Please install ffmpeg and try again."
    exit 1
fi

# Check for the required arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 input_video"
    exit 1
fi

# Input file path
input_file="$1"

# Check if the input file exists
if [ ! -f "$input_file" ]; then
    echo "Input file not found: $input_file"
    exit 1
fi

# Extract the filename and extension of the input file
filename=$(basename -- "$input_file")
filename_noext="${filename%.*}"

# Define the output file path in the specified directory
output_directory="/mnt/video_jukebox_data/morning"
output_file="$output_directory/$filename"

# Normalize audio levels using ffmpeg
ffmpeg -i "$input_file" -c:v copy -af "loudnorm=I=-24:LRA=7:TP=-1.5" "$output_file"

# Check if the normalization was successful
if [ $? -eq 0 ]; then
    echo "Audio normalized and saved as $output_file"

    # Delete the original file
    rm "$input_file"
    echo "Original file deleted: $input_file"
else
    echo "Audio normalization failed. Please check your input file and try again."
    exit 1
fi
