#!/bin/bash

# Paths
LOG_FILE=/mnt/video_jukebox_data/mpv.log
MEDIA_DIR=/mnt/video_jukebox_data/morning
OUTPUT_FILE=/tmp/unplayed.txt
video_dir="/mnt/video_jukebox_data/evening"
additional_files="/tmp/evening_playlist.txt"
temp_playlist="/tmp/temp_playlist.txt"

# Check if the log file exists
if [[ ! -f "$LOG_FILE" ]]; then
    echo "Log file $LOG_FILE not found!"
    exit 1
fi

# Get played files from the log
PLAYED_FILES=$(grep -oP '(?<=Playing: ).*' "$LOG_FILE")

# Initialize an associative array to keep track of played files
declare -A played_files_map
for file in $PLAYED_FILES; do
    played_files_map["$file"]=1
done

# Find all files in the media directory
find "$MEDIA_DIR" -type f -name "*.mp4" -o -name "*.mkv" -o -name "*.avi" > /tmp/all_files.txt

# Filter out played files to get unplayed files
> "$OUTPUT_FILE" # Create or empty the output file
while IFS= read -r file; do
    if [[ -z "${played_files_map["$file"]}" ]]; then
        echo "$file" >> "$OUTPUT_FILE"
    fi
done < /tmp/all_files.txt

# Clean up temporary file
rm /tmp/all_files.txt

echo "Unplayed files have been written to $OUTPUT_FILE"


# Create a new playlist file with the list of video files in the directory
find "$video_dir" -type f -name "*.mp4" -o -name "*.mkv" -o -name "*.avi" > $additional_files


cat $OUTPUT_FILE >> $additional_files

echo "Randomize the playlist"
shuf "$additional_files" -o "${temp_playlist}"

# Replace current playlist with new one.
echo "Replace current playlist with temp_playlist"
echo '{ "command": ["loadlist", "'"$temp_playlist"'", "replace"] }' | socat - /tmp/mpv-socket

# Set Volume at 100%
echo "Set volume at 100%"
echo '{"command": ["set_property", "volume", 100], "request_id": 0, "async": true}' | socat - /tmp/mpv-socket

# Clean up the temporary playlist file
rm $temp_playlist
rm $OUTPUT_FILE
rm $additional_files
