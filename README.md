# Video Jukebox

## Description
This project creates a simple, automated, stand alone video jukebox system displaying music video files from a local hard drive. The videos are in two categories: morning and evening. Each day at 6 am, the queue is cleared, all files in the morning category added and randomised. At 5 pm, all the files in the after evening category are added to the queue and it is randomised. 

New files are automatically processed to normalise the audio before being added to the appropriate category directory.

The use case for which it was built is running video and sound out an HDMI connection to a television at a location open 24/7.

## Installation
The project is installed on a Debian 11 or 12 system with the lightweight desktop LXDE. It expects a main hard drive and a storage hard drive. Install the OS on the main hard drive and configure the storage hard drive variable in the install script. Copy all project files into /opt/video_jukebox, modify the shell variables in install.sh, and run install.sh as root.

## Usage
Once the install is complete, add video files in /mnt/video_jukebox_data/incoming_morning or /mnt/video_jukebox_data/incoming_evening. The files will automatically be processed to normalise the audio and put them in the appropriate folder (morning or evening). 

When the machine is turned on, it will log into user video_jukebox and start randomly playing files from the morning playlist at volume of 80%. At 5 PM it will add the evening playlist files to the morning files which have not yet been played and change the volume to 100%. At 6 AM the video player is killed, a new one started with randomised files from the morning video folder at 80% volume.

The run scripts (morning_playlist.sh and evening_playlist.sh) can be modified to change the audio volume.

The core of this project is MPV. Details about that can be found <https://mpv.io/>

## License
This project is released into the public domain under the unlicense. For more information, please refer to <https://unlicense.org>

## Project status
This project is feature complete. Bug reports / patches welcome.
