#!/bin/bash

# Check if the argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

# Navigate to the specified directory
cd "$1" || exit 1


# Iterate over files and directories in the specified directory
for entry in *; do
    # Check if the entry name contains spaces, parentheses, underscores, &, `, [, or ]
    if [[ "$entry" == *" "* || "$entry" == *['('')']* || "$entry" == *'_'* || "$entry" == *'&'* || "$entry" == *'`'* || "$entry" == *'['* || "$entry" == *']'* ]]; then
        # Replace spaces with underscores, remove parentheses, ampersands, single quotes, backticks, square brackets
        new_name=$(echo "$entry" | tr ' ' '_' | tr -d '()&''`[]')
                
        # Move the entry to /mnt/video_jukebox_data/processing_morning/
        mv "$entry" "/mnt/video_jukebox_data/processing_morning/$new_name"
        
        echo "Entry moved and renamed: $entry to /mnt/video_jukebox_data/processing_morning/$new_name"
    else
        # Move the entry directly to /mnt/video_jukebox_data/processing_morning/
        mv "$entry" "/mnt/video_jukebox_data/processing_morning/$entry"
        
        echo "Entry moved: $entry to /mnt/video_jukebox_data/processing_morning/$entry"
    fi
done
