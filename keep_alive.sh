#!/bin/bash

# This script is called by a cron job every 3 minutes and restarts 
# the video jukebox if it is not running.

# Function to check if the current time is between 5 PM and 6 AM
is_evening() {
    current_hour=$(date +%H)
    if [ "$current_hour" -ge 17 ] || [ "$current_hour" -lt 6 ]; then
        return 0  # True: It is between 5 PM and 6 AM
    else
        return 1  # False: It is not between 5 PM and 6 AM
    fi
}

# Check if mpv is running
if pgrep -x "mpv" > /dev/null
then
    # If mpv is running, exit the script
    echo "mpv is running. Exiting..."
    exit 1
else
    # If mpv is not running, run /opt/video_jukebox/morning_playlist.sh
    echo "mpv is not running. Running /opt/video_jukebox/morning_playlist.sh..."
    . /opt/video_jukebox/morning_playlist.sh &
    
    # Check if it is between 5 PM and 6 AM
    if is_evening; then
        echo "It is between 5 PM and 6 AM. Running /opt/video_jukebox/evening_playlist.sh..."
        . /opt/video_jukebox/evening_playlist.sh
    fi
fi
