#!/bin/bash

# Usage: Copy all project files into /opt/video_jukebox/ Configure the variables below and run install.sh

# Assign Variables

#ZFS_Drive_ID \ get this by using ls -l '/dev/disk/by-id/
ZFS_Drive_ID=GetThisUsingAboveInstruction

# video_jukebox user password
PASSWORD=ChangeMe

# update package database

apt update

# install dependencies

apt install -y \
    ffmpeg \
    incron \
    jq \
    linux-headers-amd64 \
    zfsutils-linux \
    zfs-dkms \
    zfs-zed \
    mpv \ 
    pulseaudio \
    lxde-core \
    socat
    

    
#
# Set up ZFS drive on /mnt
#

/sbin/modprobe zfs
zpool create tank $ZFS_Drive_ID
zfs set compression=lz4 tank
zfs create -o mountpoint=/mnt tank/mnt

#
# create video_jukebox user
#
useradd -m -s /bin/bash -k /etc/skel -G sudo,audio,video,incron video_jukebox && echo "video_jukebox:$PASSWORD" | chpasswd

#
# Create video folders
#

mkdir -p /mnt/video_jukebox_data/morning
mkdir -p /mnt/video_jukebox_data/incoming_morning
mkdir -p /mnt/video_jukebox_data/processing_morning
mkdir -p /mnt/video_jukebox_data/evening
mkdir -p /mnt/video_jukebox_data/incoming_evening
mkdir -p /mnt/video_jukebox_data/processing_evening

chown -R video_jukebox:video_jukebox /mnt/video_jukebox_data


echo "Folder structure created successfully. Owner set to video_jukebox."

#
# Make all files in /opt/video_jukebox owned by user video_jukebox
#

chown -R video_jukebox:video_jukebox /opt/video_jukebox

#
# Set up crontab and incrontab from files
#

# The included crontab runs the programme morning_playlist.sh at 6 AM and evening_playlist.sh at 5 PM
crontab -u video_jukebox -l /opt/video_jukebox/video_jukebox_crontab.txt 

# Incrontab watches for new videos added and processes them with the appropriate scripts.
incrontab -u video_jukebox -l /opt/video_jukebox/video_jukebox_incrontab.txt

#
# Set up lightdm to automatically log in as user video_jukebox when the computer is started
#

sed -i 's/^#autologin-guest .*/autologin-guest = false/' /etc/lightdm/lightdm.conf
sed -i 's/^#autologin-user .*/autologin-user = video_jukebox/' /etc/lightdm/lightdm.conf
sed -i 's/^#autologin-user-timeout .*/autologin-user-timeout = 0/' /etc/lightdm/lightdm.conf

#
# Set up the autostart file so that it runs the morning playlist when the computer is started.
#

mkdir -p 
echo -e "@lxpanel --profile LXDE\n@pcmanfm --desktop --profile LXDE\n@xscreensaver -no-splash\n/opt/video_jukebox/keep_alive.sh" > /home/video_jukebox/.config/lxsession/LXDE/autostart
chmod +x /home/video_jukebox/.config/lxsession/LXDE/autostart
chown video_jukebox:video_jukebox /home/video_jukebox/.config/lxsession/LXDE/autostart

echo "Autostart file set up to play morning playlist when computer is turned on"
