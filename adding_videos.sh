#!/bin/bash

#
# This section manages adding new videos to the system.
#

# Install incrontab entries to monitor the incoming directories for new videos.
# When new videos appear, they are processed by the normalise scripts.

#
# Monitor incoming folders and remove special characters from file names if necessary
#

#Define the directory to monitor
WATCHED_DIR="/mnt/video_jukebox_data/incoming_morning"

# Define the script to execute
SCRIPT_PATH="/opt/video_jukebox/clean_filenames_morning.sh"

# Create the script to install incrontab entry
echo "\"$WATCHED_DIR\" IN_MOVED_TO,IN_CLOSE_WRITE \"$SCRIPT_PATH\" \$@" >> incrontab_entry

#Define the directory to monitor
WATCHED_DIR="/mnt/video_jukebox_data/incoming_evening"

# Define the script to execute
SCRIPT_PATH="/opt/video_jukebox/clean_filenames_evening.sh"

# Create the script to install incrontab entry
echo "\"$WATCHED_DIR\" IN_MOVED_TO,IN_CLOSE_WRITE \"$SCRIPT_PATH\" \$@" >> incrontab_entry

#
# Monitor processing folders, normalise files that appear, and move them to final folders
#


#Define the directory to monitor
WATCHED_DIR="/mnt/video_jukebox_data/processing_morning"

# Define the script to execute
SCRIPT_PATH="/opt/video_jukebox/normalise_audio_morning.sh"

# Create the script to install incrontab entry
echo "\"$WATCHED_DIR\" IN_MOVED_TO,IN_CLOSE_WRITE \"$SCRIPT_PATH\" \$@/\$#" >> incrontab_entry

#Define the directory to monitor
WATCHED_DIR="/mnt/video_jukebox_data/processing_evening"

# Define the script to execute
SCRIPT_PATH="/opt/video_jukebox/normalise_audio_evening.sh"

# Create the script to install incrontab entry
echo "\"$WATCHED_DIR\" IN_MOVED_TO,IN_CLOSE_WRITE \"$SCRIPT_PATH\" \$@/\$#" >> incrontab_entry

# Install the incrontab entry
incrontab incrontab_entry

# Remove the temporary incrontab entry file
rm incrontab_entry

echo "Incrontab entry for processing new video files installed successfully."
